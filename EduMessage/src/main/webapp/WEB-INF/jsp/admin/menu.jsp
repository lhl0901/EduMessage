<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked" id="nav">
        <li><a href="<c:url value="/admin/showCourse"/>">课程管理<span class="badge pull-right">8</span></a></li>
        <li><a href="<c:url value="/admin/showStudent"/>">学生管理<span class="badge pull-right">59</span></a></li>
        <li><a href="<c:url value="/admin/showTeacher"/>">教师管理<span class="badge pull-right">10</span></a></li>
        <li><a href="<c:url value="/admin/userPasswordRest"/>">账号密码重置<sapn class="glyphicon glyphicon-repeat pull-right"></sapn></a></li>
        <li><a href="<c:url value="/admin/passwordRest"/>">修改密码<sapn class="glyphicon glyphicon-pencil pull-right"></sapn></a></li>
        <li><a href="${pageContext.request.contextPath}/logout">退出系统<sapn class="glyphicon glyphicon-log-out pull-right"></sapn></a></li>
        <li class="disabled"><a href="##">Responsive</a></li>
    </ul>
</div>